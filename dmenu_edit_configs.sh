#!/bin/sh

#a dmenu script with shortcuts to my most edited config files

declare -a options=(" config.py 
 zshrc 
 vimrc 
 sxhkdrc 
 Xdefaults 
 compton.conf 
 autostart.sh 
 directories.txt 
 keys.txt ")

choice=$(echo -e "${options[@]}" | dmenu -m 0 -i -p 'Edit a config file: ')

	if [ "$choice" == ' config.py ' ]; then
        exec st -e vim $HOME/.dotfiles/config.py
	fi
	if [ "$choice" == ' zshrc ' ]; then
        exec st -e vim $HOME/.dotfiles/.zshrc
	fi
	if [ "$choice" == ' vimrc ' ]; then
        exec st -e vim $HOME/.dotfiles/.vimrc
	fi
	if [ "$choice" == ' sxhkdrc ' ]; then
        exec st -e vim $HOME/.dotfiles/.sxhkdrc
	fi
	if [ "$choice" == ' Xdefaults ' ]; then
        exec st -e vim $HOME/.dotfiles/.Xdefaults
	fi
	if [ "$choice" == ' compton.conf ' ]; then
        exec st -e vim $HOME/.dotfiles/compton.conf
	fi
	if [ "$choice" == ' autostart.sh ' ]; then
        exec st -e vim $HOME/.dotfiles/autostart.sh
	fi
	if [ "$choice" == ' directories.txt ' ]; then
        exec st -e vim $HOME/.dotfiles/directories.txt
	fi
	if [ "$choice" == ' keys.txt ' ]; then
        exec st -e vim $HOME/.dotfiles/keys.txt
	fi
	
