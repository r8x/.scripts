#!/bin/sh

# a little dmenu script to start a browser with my most used websites

declare -a options=(" Mailo 
 gSheets 
 gCalendar 
 gDrive 
 gNotes 
 gMaps 
 Youtube 
 Amazon 
 Bitbucket ")

choice=$(echo -e "${options[@]}" | dmenu -m 0 -i -p 'Where to go? ')

	if [ "$choice" == ' Mailo ' ]; then
        exec firefox https://www-1.mailo.com/mailo/mail/inbox.php?s=TXMzqTBZDJBrknLxovXy9Ech&_c=1569792584 
	fi
	if [ "$choice" == ' gSheets ' ]; then
        exec firefox https://docs.google.com/spreadsheets/u/0/ 
	fi
	if [ "$choice" == ' gCalendar ' ]; then
        exec firefox https://calendar.google.com/calendar/r?tab=mc&pli=1 
	fi
	if [ "$choice" == ' gDrive ' ]; then
        exec firefox https://drive.google.com/drive/my-drive 
	fi
	if [ "$choice" == ' gNotes ' ]; then
        exec firefox https://keep.google.com/u/0/ 
	fi
	if [ "$choice" == ' gMaps ' ]; then
        exec firefox https://maps.google.com 
	fi
	if [ "$choice" == ' Youtube ' ]; then
        exec firefox https://www.youtube.com 
	fi
	if [ "$choice" == ' Amazon ' ]; then
        exec firefox https://smile.amazon.de/ 
	fi
	if [ "$choice" == ' Bitbucket ' ]; then
        exec firefox https://bitbucket.org/ 
	fi
