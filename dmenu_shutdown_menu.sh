#!/bin/sh

# a dmenu script with a shutdown/reboot dialog

declare -a options=(" shutdown 
 reboot ")

choice=$(echo -e "${options[@]}" | dmenu -m 0 -i -p 'shutdown or reboot?')

	if [ "$choice" == ' shutdown ' ]; then
        exec shutdown -h now
	fi
	if [ "$choice" == ' reboot ' ]; then
        exec reboot 
	fi
	
