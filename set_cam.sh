#!/bin/sh
#
# set the webcam for streaming
#
v4l2-ctl -d /dev/video0 --set-ctrl=brightness=110
v4l2-ctl -d /dev/video0 --set-ctrl=focus_auto=0
v4l2-ctl -d /dev/video0 --set-ctrl=white_balance_temperature_auto=0
